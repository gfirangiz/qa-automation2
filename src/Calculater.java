import java.util.Scanner;

public class Calculater {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        double result;

        System.out.println("Enter first number:");
        double num1 = input.nextInt();

        System.out.println("Enter operator:");
        String operator = input.next();

        System.out.println("Enter second number:");
        double num2 = input.nextInt();

        switch(operator) {
            case "+":
                result = num1+num2;
                System.out.println("Result: "+result);
                break;
            case "-":
                result = num1-num2;
                System.out.println("Result: "+result);
                break;
            case "*":
                result = num1*num2;
                System.out.println("Result: "+result);
                break;
            case "/":
                result = num1/num2;
                System.out.println("Result: "+result);
                break;
        }
    }
}
